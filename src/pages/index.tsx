import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import type { InvalidateQueryFilters } from "@tanstack/react-query";
import { format } from "date-fns";
import Head from "next/head";

import { useState, useCallback } from "react";
import { Toaster } from "~/components/ui/toaster";
import { useToast } from "~/components/ui/use-toast";

import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "~/components/ui/popover";

import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "~/components/ui/sheet";

import Layout from "~/components/layout";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "~/components/ui/table";
import { fetchData } from "~/utils";
import type { ReturnType } from "./api/voyage/getAll";
import { Button } from "~/components/ui/button";
import { TABLE_DATE_FORMAT } from "~/constants";
import MainForm from "~/components/MainPage/MainForm";
import { Badge } from "~/components/ui/badge";

export default function Home() {
  const { data: voyages } = useQuery<ReturnType>({
    queryKey: ["voyages"],

    queryFn: () => fetchData("voyage/getAll"),
  });

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const [isCreateOpen, setIsCreateOpen] = useState(false);

  const close = useCallback(() => {
    setIsCreateOpen(false);
  }, []);

  const mutation = useMutation({
    mutationFn: async (voyageId: string) => {
      const response = await fetch(`/api/voyage/delete?id=${voyageId}`, {
        method: "DELETE",
      });

      if (!response.ok) {
        throw new Error("Failed to delete the voyage");
      }
    },
    onSuccess: async () => {
      await queryClient.invalidateQueries([
        "voyages",
      ] as InvalidateQueryFilters);
    },
  });

  const handleDelete = (voyageId: string) => {
    mutation.mutateAsync(voyageId).catch((error) => {
      toast({
        variant: "error",
        title: error?.message ?? "Failed to delete the voyage",
      });
    });
  };

  return (
    <>
      <Head>
        <title>Voyages | DFDS</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <div className="w-full ">
          <Sheet open={isCreateOpen} onOpenChange={setIsCreateOpen}>
            <SheetTrigger asChild>
              <Button variant="secondary" className="mt-2.5">
                Create
              </Button>
            </SheetTrigger>
            <SheetContent>
              <SheetHeader>
                <SheetTitle>Create Voyage</SheetTitle>
                <SheetDescription>
                  Please fill the form and push Create button
                </SheetDescription>
              </SheetHeader>
              <MainForm close={close} />
            </SheetContent>
          </Sheet>
          <Table>
            <TableHeader>
              <TableRow>
                <TableHead>Departure</TableHead>
                <TableHead>Arrival</TableHead>
                <TableHead>Port of loading</TableHead>
                <TableHead>Port of discharge</TableHead>
                <TableHead>Vessel</TableHead>
                <TableHead>Unit Types</TableHead>
                <TableHead>&nbsp;</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {voyages?.map((voyage) => (
                <TableRow key={voyage.id}>
                  <TableCell>
                    {format(
                      new Date(voyage.scheduledDeparture),
                      TABLE_DATE_FORMAT,
                    )}
                  </TableCell>
                  <TableCell>
                    {format(
                      new Date(voyage.scheduledArrival),
                      TABLE_DATE_FORMAT,
                    )}
                  </TableCell>
                  <TableCell>{voyage.portOfLoading}</TableCell>
                  <TableCell>{voyage.portOfDischarge}</TableCell>
                  <TableCell>{voyage.vessel.name}</TableCell>
                  <TableCell>
                    <Popover>
                      <PopoverTrigger>
                        <Badge variant="secondary" className="mb-1 mr-1">
                          {voyage.unitTypes.length}
                        </Badge>
                      </PopoverTrigger>
                      <PopoverContent className="bg-gray-800">
                        <div className="mb-1 text-lg">Unit Types</div>
                        {voyage.unitTypes.map((unitType) => (
                          <div
                            className="grid grid-cols-5 items-center gap-4 py-0.5"
                            key={unitType.id}
                          >
                            <div className="col-span-3">{unitType.name}</div>
                            <div className="col-span-2">
                              {unitType.defaultLength}
                            </div>
                          </div>
                        ))}
                      </PopoverContent>
                    </Popover>
                  </TableCell>
                  <TableCell>
                    <Button
                      onClick={() => handleDelete(voyage.id)}
                      variant="outline"
                    >
                      X
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </Layout>
      <Toaster />
    </>
  );
}
