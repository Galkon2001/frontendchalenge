import { useForm } from "react-hook-form";
import { useMemo } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import type { z } from "zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import type { InvalidateQueryFilters } from "@tanstack/react-query";

import { fetchData, postData } from "~/utils";

import type { VesselsType } from "../../pages/api/vessel/getAll";
import type { VesselsUnitTypes } from "../../pages/api/unitType/getAll";

import formSchema from "./mainFormSchema";

import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "~/components/ui/select";
import { MultiSelect } from "~/components/ui/multiSelect";
import { Input } from "~/components/ui/input";
import {
  Form,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
  FormField,
} from "~/components/ui/form";
import { useToast } from "~/components/ui/use-toast";
import { Button } from "~/components/ui/button";
import { SheetFooter } from "~/components/ui/sheet";
import type { MainFormProps } from "./types";

const MainForm = ({ close }: MainFormProps) => {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const { data: vessels } = useQuery<VesselsType>({
    queryKey: ["vessels"],

    queryFn: () => fetchData("vessel/getAll"),
  });

  const { data: unitTypesData } = useQuery<VesselsUnitTypes>({
    queryKey: ["unitTypes"],

    queryFn: () => fetchData("unitType/getAll"),
  });

  const unitTypes = useMemo(
    () =>
      unitTypesData?.map(({ id, name }) => ({ value: id, label: name })) ?? [],
    [unitTypesData],
  );

  const voyageMutation = useMutation({
    mutationFn: async (values: z.infer<typeof formSchema>) => {
      const {
        portOfLoading,
        portOfDischarge,
        vesselId,
        scheduledDepartureDay,
        scheduledDepartureTime,
        scheduledArrivalDay,
        scheduledArrivalTime,
        unitTypes,
      } = values;

      const response = await postData("voyage/create", {
        portOfLoading,
        portOfDischarge,
        vessel: vesselId,
        departure: new Date(
          `${scheduledDepartureDay}T${scheduledDepartureTime}`,
        ).toISOString(),
        arrival: new Date(
          `${scheduledArrivalDay}T${scheduledArrivalTime}`,
        ).toISOString(),
        unitTypes,
      });

      if (!response.ok) {
        throw new Error("Failed to create voyage");
      }
    },
    onSuccess: async () => {
      await queryClient.invalidateQueries([
        "voyages",
      ] as InvalidateQueryFilters);
    },
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      portOfLoading: "",
      portOfDischarge: "",
      vesselId: "",
      scheduledDepartureDay: "",
      scheduledDepartureTime: "",
      scheduledArrivalDay: "",
      scheduledArrivalTime: "",
      unitTypes: [],
    },
  });

  const onSubmit = (values: z.infer<typeof formSchema>) => {
    voyageMutation
      .mutateAsync(values)
      .then(() => {
        toast({
          variant: "success",
          title: "Voyage Created",
          description: `From ${values.portOfLoading} to ${values.portOfDischarge}`,
        });
        close();
        form.reset();
      })
      .catch(() => {
        toast({
          variant: "error",
          title: "Voyage Creating failed",
        });
      });
  };

  return (
    <Form {...form}>
      <form
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        onSubmit={form.handleSubmit(onSubmit)}
      >
        <FormField
          control={form.control}
          name="portOfLoading"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Port Of Loading</FormLabel>
              <FormControl>
                <Input
                  placeholder="Type..."
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="portOfDischarge"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Port Of Discharge</FormLabel>
              <FormControl>
                <Input
                  placeholder="Type..."
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="vesselId"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Vessel</FormLabel>
              <Select onValueChange={field.onChange} defaultValue={field.value}>
                <FormControl className="col-span-3">
                  <SelectTrigger>
                    <SelectValue placeholder="Select a vessel" />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {vessels?.map((vessel) => (
                    <SelectItem key={vessel.value} value={vessel.value}>
                      {vessel.label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="unitTypes"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">Unit Types</FormLabel>
              <div className="col-span-3">
                <MultiSelect
                  selected={field.value}
                  options={unitTypes}
                  onChange={field.onChange}
                />
              </div>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="scheduledDepartureDay"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">
                Scheduled Departure Day
              </FormLabel>
              <FormControl>
                <Input
                  type="date"
                  placeholder="Type..."
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="scheduledDepartureTime"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">
                Scheduled Departure Time
              </FormLabel>
              <FormControl>
                <Input
                  type="time"
                  placeholder="Type..."
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="scheduledArrivalDay"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">
                Scheduled Arrival Day
              </FormLabel>
              <FormControl>
                <Input
                  type="date"
                  placeholder="Type..."
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="scheduledArrivalTime"
          render={({ field }) => (
            <FormItem className="grid grid-cols-4 items-center gap-4">
              <FormLabel className="text-right">
                Scheduled Arrival Time
              </FormLabel>
              <FormControl>
                <Input
                  placeholder="Type..."
                  type="time"
                  {...field}
                  className="col-span-3"
                />
              </FormControl>
              <FormMessage className="col-span-4 mx-auto" />
            </FormItem>
          )}
        />
        <SheetFooter>
          <Button
            disabled={voyageMutation.isPending}
            className="mx-auto mt-2.5"
            type="submit"
          >
            Create
          </Button>
        </SheetFooter>
      </form>
    </Form>
  );
};

export default MainForm;
