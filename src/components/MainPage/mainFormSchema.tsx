import { isAfter, isEqual } from "date-fns";
import { z } from "zod";

const formSchema = z
  .object({
    portOfLoading: z
      .string()
      .min(1, { message: "Port Of Loading is required" }),
    portOfDischarge: z
      .string()
      .min(1, { message: "Port Of Discharge is required" }),
    vesselId: z.string().min(1, { message: "Vessel is required" }),
    unitTypes: z.string().array().min(5, { message: "Choose at least 5" }),
    scheduledDepartureDay: z.string().min(1, { message: "Field required" }),
    scheduledDepartureTime: z.string().min(1, { message: "Field required" }),
    scheduledArrivalDay: z.string().min(1, { message: "Field required" }),
    scheduledArrivalTime: z.string().min(1, { message: "Field required" }),
  })
  .refine(
    ({ scheduledDepartureDay, scheduledArrivalDay }) =>
      isAfter(scheduledArrivalDay, scheduledDepartureDay) ||
      isEqual(scheduledArrivalDay, scheduledDepartureDay),
    {
      message: "Arrive Day cant be before Departure",
      path: ["scheduledArrivalDay"],
    },
  )
  .refine(
    ({
      scheduledDepartureDay,
      scheduledDepartureTime,
      scheduledArrivalDay,
      scheduledArrivalTime,
    }) =>
      !isEqual(scheduledArrivalDay, scheduledDepartureDay) ||
      scheduledArrivalTime
        .split(":")
        .map(Number)
        .reduceRight((acc, cur) => acc + cur * 60) >
        scheduledDepartureTime
          .split(":")
          .map(Number)
          .reduceRight((acc, cur) => acc + cur * 60),
    {
      message: "Arrive Time must be after Departure",
      path: ["scheduledArrivalTime"],
    },
  );

export default formSchema;
